const meta = {
  <class name>: {
    init: () => {
      // Initial values to copy to memory
      const xMin = 0
      const xMax = 100
      const yMin = 0
      const yMax = 20
      return {
        transform: [xMin, xMax, yMin, yMax]
      }
    },
    run: function(memory){
      console.log("run")
    },
    render: function(gfx, memory){
      // console.log("render")
    },
    event: {
      onmousedown: function(memory){ return () => console.log("onmousedown") },
      onmouseup: function(memory){ return () => console.log("onmouseup") }
    }
  }
}

ENGINE.buildEngines(meta).forEach(e => e.start())