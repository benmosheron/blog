
function coinFlip(){
  return Math.random() > 0.5 ? 1 : -1
}
function rand255(){
  return Math.floor(Math.random() * 256)
}
function iterate(v){ return v + (v * coinFlip() / 100) }

function updateExternalAggregateValues(memory){
  // HAX ALERT - will not play nice if more than one walk element is loaded
  if(!memory.preRun){
    memory.preRun = true
    return
  }
  memory.external.n++
  memory.external.total += memory.v
  if(memory.v > 1) { memory.external.winCount++ }
  const winPercentage = 100 * memory.external.winCount / memory.external.n
  const average = memory.external.total / memory.external.n
  document.getElementById("numberOfWalks").innerHTML = memory.external.n.toString()
  document.getElementById("winPercentage").innerHTML = winPercentage.toString().slice(0,4) + "%"
  document.getElementById("averageValue").innerHTML = "£" + average.toString().slice(0,4)
}
function resetMemory(memory){
  memory.i = 0
  memory.v = memory.vInit
  memory.r = rand255()
  memory.g = rand255()
  memory.b = rand255()
}
function fillIntoMemory(memory){
  for (let j = 0; j < memory.vs.length; j++) {
    memory.vs[j] = memory.v
    memory.v = iterate(memory.v)
  }
}
function increment(memory){
  memory.i = memory.i + memory.vs.length
}
function render(gfx, memory){
  memory.vs.forEach((v,i) => {
    const p = gfx.transform(memory.i + i - memory.vs.length, v)
    gfx.square(p.x, p.y, memory.s, `rgb(${memory.r},${memory.g},${memory.b})`)
  })
}
function renderLine(gfx, memory){
  const start = gfx.transform(0,1)
  const finish = gfx.transform(memory.transform[1],1)
  gfx.line(start.x, start.y, finish.x, finish.y, 1, "rgb(150,150,152)")
}

function resizeMemoryHack(memory, newSize){    
  const vsNew = new Array(newSize)
  for(let i=0;i < memory.vs.length && i < newSize; i++){
      vsNew[i] = memory.vs[i]
  }
  memory.vs = vsNew
}

const meta = {
  walk: {
    init: () => {
      // Initial values to copy to memory
      const vInit = 1
      const xMin = 0
      const xMax = 10000
      const yMin = 0
      const yMax = 20
      const perFrameInit = 100
      const perFrameFF = 10000
      return {
        transform: [xMin, xMax, yMin, yMax],
        // Walk value
        vInit: vInit,
        v: vInit,
        perFrameInit: perFrameInit,
        perFrameFF: perFrameFF,
        vs: new Array(perFrameInit).fill(vInit),
        // Index (x direction), start at the end to trigger a reset
        i: xMax,
        // Number of steps
        n: xMax,
        // Colour
        r: 210,
        g: 30,
        b: 210,
        // Size
        s: 2,
        // For aggregate values
        external: {
          preRun: true,
          n: 0,
          total: 0,
          winCount: 0
        }
      }
    },
    run: function(memory){
      if(memory.i >= memory.n){
        updateExternalAggregateValues(memory)
        resetMemory(memory)
      }
      fillIntoMemory(memory)
      increment(memory)
    },
    render: function(gfx, memory){
      render(gfx, memory)
      renderLine(gfx, memory)
    },
    event: {
      onmousedown: function(memory){ return () => resizeMemoryHack(memory, memory.perFrameFF) },
      onmouseup: function(memory){ return () => resizeMemoryHack(memory, memory.perFrameInit) }
    }
  },
  fade: {
    init: function(){ return { every: 1, i:0 } },
    run: function(memory, fpsS){},
    render: function(gfx, memory){
      if(memory.i % memory.every === 0){
        gfx.rect(0,0,gfx.width,gfx.height,"rgba(255,255,255,0.01)")
      }
      memory.i++
    }
  },
  all: allMeta()
}

function allMeta(){ return {
  init: function(){ return { 
      // first iteration has one point at x=1
      vals: [1],
      i:0,
      transform: [0,10,0,3],
      stop:10
    }
  },
  run: function(memory, fpsS){
    if(memory.i == memory.stop) return;
    memory.vals = memory.vals.flatMap()
  },
  render: function(gfx, memory){
    memory.vals.forEach(v => {
      const t = gfx.transform(memory.i,v)
      gfx.square(t.x, t.y,2,"rgb(255,0,0)")  
    })
  }
}}

ENGINE.buildEngines(meta).forEach(e => e.start())
