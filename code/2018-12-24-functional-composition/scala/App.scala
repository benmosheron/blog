object App {
def main(args: Array[String]): Unit = {

def addOne(x: Int): Int = x + 1
def square(x: Int): Int = x * x
def double(x: Int): Int = x * 2
// Doesn't work:
// def allThree = addOne compose square compose double
// But this does:
def allThree = (addOne _) compose (square _) compose (double _)
println("allThree(10): " + allThree(10))

// Define functions which do the same things.
val f = ((x: Int) => x + 1)
val g = ((x: Int) => x * x)
val h = ((x: Int) => x * 2)
val fgh = f compose g compose h
println
println("fgh(10): " + fgh(10))


// Demonstrate the compiler converting a method to a function.
// Assign a method to a function
val fAddOne: Int => Int = addOne

// We need to manually convert the first compiled method to a function.
// Compiler will convert methods square and double to functions.
def allThreeA = (addOne _) compose square compose double
def allThreeB = fAddOne compose square compose double
// Remember that the infix notation hides the fact that "compose"
// is itself a method, belonging to the Function1 trait
def allThreeC = fAddOne.compose(square).compose(double)
println
println("allThreeA(10): " + allThreeA(10))
println("allThreeB(10): " + allThreeB(10))
println("allThreeC(10): " + allThreeC(10))

// A compose example

// We need functions, not methods.
// val fAddOne: Int => Int = addOne _// defined earlier
val fSquare: Int => Int = square _
val fDouble: Int => Int = double _

// A range of ints
val ints = 1 to 10

// Lets do a bunch of different combinations of functions to them.

// Just double them:
val doubled = ints.map(fDouble)
// Double then add one:
val doubledAddOne = ints.map(fAddOne compose fDouble)
// Add one then double:
val addOneDoubled = ints.map(fDouble compose fAddOne)
// Add one, then double, then square:
val oneDubSq = ints.map(fSquare compose fDouble compose fAddOne)
// Square, then double, then add one:
val sqDubOne = ints.map(fAddOne compose fDouble compose fSquare)
// same as:
//val sqDubOne = ints.map(fSquare andThen fDouble andThen fAddOne)
println
println("doubled:       " + doubled)
println("doubledAddOne: " + doubledAddOne)
println("addOneDoubled: " + addOneDoubled)
println("oneDubSq:      " + oneDubSq)
println("sqDubOne:      " + sqDubOne)

// Trying to mimic F#?
def <<[A,B,C](f: B => C, g: A => B): A => C = f.compose(g)

// Yuck, I wish I had never tried, it really needs infix notation.
def allThreeD = <<(addOne, <<(square, double))

// Cats below!

// Import the Arrow type class
import cats.arrow.Arrow
// Import implicit values.
// I think we only need an Arrow instance for Function1, which comes from:
// import cats.instances.function._
// And the syntax for the >>> and <<< functions, which comes from:
// import cats.syntax.arrow._
// But importing those two doesn't work, if you know why, please tell me!
// Instead we'll import everything:
import cats.implicits._
val aAddOne = Arrow[Function1].lift(addOne)
val aSquare = Arrow[Function1].lift(square)
val aDouble = Arrow[Function1].lift(double)
val aDoubleSquareAddOne1 = ints.map(aAddOne <<< aSquare <<< aDouble)
val aDoubleSquareAddOne2 = ints.map(aDouble >>> aSquare >>> aAddOne)
println
println("aDoubleSquareAddOne1: " + aDoubleSquareAddOne1)
println("aDoubleSquareAddOne2: " + aDoubleSquareAddOne2)

// Bonus: we only need an arrow instance for the first compiled arrow, 
// and the compiler is smart enough to convert a function into an arrow
// for us! We just have to provide a function to kick things off.
// The compiler can take care of the rest - remember, double and addOne
// are our original methods, defined right at the top!
val bonus = ints.map((square _) <<< double <<< addOne)
println
println("bonus:" + bonus)

// Edit (Thursday 27th December)
// We can use an implicit class to get the function we want
import FunctionExtensions._
// Backward composition
val edit1 = fAddOne <+ double <+ double
// Forward composition
val edit2 = fDouble +> (x => x * x * x)
println
println("Edit")
println("edit1(7): " + edit1(7))
println("edit2(2): " + edit2(2))

}
}

object FunctionExtensions {
  implicit class Compose[A,B](val self: Function1[A,B]) extends AnyVal {
    def <+[C](g: Function1[C,A]): Function1[C,B] = self.compose(g)
    def +>[C](g: Function1[B,C]): Function1[A,C] = self.andThen(g)
  }
}