To run these commands, your terminal must be in the same folder as this file (`cd` to this directory).
This is _very important_ because the java compiler is picky about things like names, packages and paths

Two java files:
- `com/benmosheron/printing/Printing.java` has no dependencies
- `com/benmosheron/scalaforjava/JavaApp.java` depends on `Printing.java` and `RuntimeInteface.java` which is included in `lib/compile/Compile.jar`

In both cases, the path to the source file from this directory is exactly the same as the source files package (replacing `/` with `.`).

Because it has no dependencies, Printing.java can be compiled like so:

`javac -d out com/benmosheron/printing/Printer.java`
- `-d out` says to put the compilation results (class files) in the `out` directory
- `com/benmosheron/printing/Printing.java` path to source file

This will create `com/benmosheron/printing/Printing.class` in the `out` directory.

To compile JavaApp using javac:
`javac -d out -cp out:lib/compile/Compile.jar com/benmosheron/printing/Printer.java com/benmosheron/scalaforjava/SimpleJavaApp.java com/benmosheron/scalaforjava/JavaApp.java`
- `-d out` says to put the compilation results (class files) in the `out` directory
- `-cp out:lib/compile/Compile.jar` let's break this down
   - `out` puts the `out` directory on the classpath. Java will look in `out` for compile dependencies
   - `:` include another item in the classpath
   - `lib/compile/Compile.jar` adds all the classes inside Compile.jar onto the classpath. Note that we include the jar file name, because the jar contains it's own root directory.
- `com/benmosheron/scalaforjava/JavaApp.java` path to source files to compile

Run using:

`java -cp out:lib/compile/Compile.jar:lib/runtime/Runtime.jar com.benmosheron.scalaforjava.JavaApp`

Or, pack into a jar file (specifying class path and main class in manifest.txt):

`jar cvfm JavaApp.jar manifest.txt -C out com`

Run with:

`java -jar JavaApp.jar`
