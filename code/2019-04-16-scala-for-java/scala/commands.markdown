Compile scala app, putting generated class files into the out directory.

`scalac -d out -cp lib/compile/Compile.jar com/benmosheron/scalaforjava/ScalaApp.scala`

Run the compiled app using scala:

`scala -cp out:lib/compile/Compile.jar com.benmosheron.scalaforjava.ScalaApp`

Run with java (replace `lib/runtime/scala-library.jar` with the path to the scala runtime if it is elsewhere):

`java -cp out:lib/compile/Compile.jar:lib/runtime/scala-library.jar com.benmosheron.scalaforjava.ScalaApp`

Build a jar (change the path to the scala runtime in `manifest.txt` if it is elsewhere):

`jar cvfm ScalaApp.jar manifest.txt -C out com`

Run with java:

`java -jar ScalaApp.jar`