---
layout: post
title:  "A Random Walk"
# date:   2019-01-24 20:51:35 +0000
tags: []
---


Start with £1 in the bank and flip a coin:
- heads, your bank balance increases by 1%
- tails, it decreases by 1%

Keep flipping the coin, so for example, if you get lucky and start with two heads, you'll have £1.0201 (not £1.02).
Repeat until you've flipped the coin 10,000 times. What is the more likely outcome?

1. you end up with more than you started with
2. you end up with less than you started with
3. both are equally likely

<div class="pad-lower">
  <canvas id="walkCanvas0" class="engine walk fade border" height="1000px" width="800px"></canvas>
</div>

Above is a simulation of this scenario. Each line represents a person flipping that coin 10,000 times. To see which is more likely, I'm keeping a running count of how many people finish above and below the line (drawn at £1.00).

<div class="pad-lower">
So far, <span id="numberOfWalks">-</span> simulations have been run. <span id="winPercentage">-</span> of them have won, and the mean value (truncated down to the nearest pence) in each bank account after the 10,000th flip is <span id="averageValue">-</span>.
</div>

Let it run for a while (click the simulation and hold to fast forward) and a pattern emerges: it is more likely to lose money than gain. I was surprised by this, intuitively I expected both to be equally likely. Let's explore why this is the case.

# Follow Up Question

I had to be a little careful with the way I worded the question, we end up with a different result if I instead ask: 

What is the expected value in the bank account after 10,000 coin flips?

1. greater than £1.00
2. less than £1.00
3. exactly £1.00

As the simulation suggests, the correct answer here is exactly £1.00.

# Intuition

Do these results not contradict each other? Well, no! The reason being that the probability distribution is skewed. While ending up with less money is more likely, it's possible to win much more than you can lose.

Consider two people with remarkably different amounts of luck. One is having a rough day, and, unable to catch a break, flips 10,000 tails in a row, leaving them with about £0. Another, on an equally unlikely roll, has flipped 10,000 heads. Their bank balance has grown exponentially to a value so astronomically high that the British pound is in turmoil, economics has totally broken down.


<div class="pad-lower">
  <canvas id="allValsCanvas0" class="engine all border" height="300px" width="800px"></canvas>
</div>

<style>

canvas {
  cursor: pointer;
  padding-left: 0;
  padding-right: 0;
  margin-left: auto;
  margin-right: auto;
  display: block;
}

canvas.border{  
  border: solid;
  border-color: rgb(150,150,150);
}

div.pad-lower{
  padding-bottom: 10px;
}

</style>

<script>
  {% include engine.js %}
  {% include random-walk.js %}
</script>